import java.util.Scanner;

public class StructureMenu {
    private EmployeeList list;

    StructureMenu(EmployeeList list) {
        this.list = list;
    }
    StructureMenu invoke() {
        int parMenu;
        do {
            MainMenu mainMenu = new MainMenu().invoke();
            parMenu = mainMenu.getParMenu();
            Scanner menu = mainMenu.getMenu();
            switch (parMenu) {
                case 1:
                    //Ввести данные о сотруднике
                    list.addEmployee();
                    break;
                case 2:
                    //Вывести данные о сотрудниках
                    list.conclusionEmployee();
                    break;
                case 3:
                    //Сортировать список сотрудников
                    structuraSortMenu(menu);
                    break;
                case 4:
                    //Поиск сотрудников по должности
                    list.searchAppointment(list);
                    break;
                case 5:
                    //Выход
                    System.out.println("Досвидание.");
                    break;
            }
            System.out.println();
        } while ((parMenu == 1) || (parMenu == 2) || (parMenu == 3) || (parMenu == 4));
        return null;
    }

    private void structuraSortMenu(Scanner menu) {
        int field = new SortMenu(menu).invoke();
        switch (field) {
            case 1:
                list.sort(EmployeeList.SORT_NAME);
                System.out.println("\n Сортировка по Имени");
                break;
            case 2:
                list.sort(EmployeeList.SORT_SURNAME);
                System.out.println("\n Сортировка по Фамилии");
                break;
            case 3:
                list.sort(EmployeeList.SORT_APPOINTMENT);
                System.out.println("\n Сортировка по Должности");
                break;
            case 4:
                list.sort(EmployeeList.SORT_SALARY);
                System.out.println("\n Сортировка по Зарплате");
                break;
        }
    }
}

