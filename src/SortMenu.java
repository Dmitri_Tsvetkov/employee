import java.util.Scanner;

public class SortMenu {
        private Scanner menu;

        SortMenu(Scanner menu) {
            this.menu = menu;
        }

        public int invoke() {
            System.out.println("Выберите по какому полю будем сортировать сотрудников: ");
            System.out.println("1.По Имени.");
            System.out.println("2.По Фамилии.");
            System.out.println("3.По Должности.");
            System.out.println("4.По Зарплате.");
            return menu.nextInt();
        }
}

class MainMenu {
    private int parMenu;
    private Scanner menu;

    public int getParMenu() {
        return parMenu;
    }

    public Scanner getMenu() {
        return menu;
    }

    public MainMenu invoke() {
        System.out.println("Меню:");
        System.out.println("1.Добавить нового работника.");
        System.out.println("2.Вывести данные о сотрудниках.");
        System.out.println("3.Сортировать список сотрудников.");
        System.out.println("4.Поиск сотрудников по должности.");
        System.out.println("5.Выход.");
        menu = new Scanner(System.in);
        parMenu = menu.nextInt();
        return this;
    }
}


