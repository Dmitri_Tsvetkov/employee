
public class Employee {
    private String name, surname, appointment;
    private int salary;

    Employee(String name, String surname, String appointment, int salary) {
        this.name = name;
        this.surname = surname;
        this.appointment = appointment;
        this.salary = salary;
    }

    public String getName() {
        return name;
    }

    public String getSurname() {
        return surname;
    }

    public String getAppointment() {
        return appointment;
    }

    public int getSalary() {
        return salary;
    }
}
