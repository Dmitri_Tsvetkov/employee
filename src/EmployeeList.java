import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Scanner;

public class EmployeeList {

    public static final Comparator<Employee> SORT_NAME = Comparator.comparing(Employee::getName);
    public static final Comparator<Employee> SORT_SURNAME = Comparator.comparing(Employee::getSurname);
    public static final Comparator<Employee> SORT_APPOINTMENT = Comparator.comparing(Employee::getAppointment);
    public static final Comparator<Employee> SORT_SALARY = Comparator.comparing(Employee::getSalary);

    private List<Employee> list = new ArrayList<Employee>();

    public void addEmployee() {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Введите Имя");
        String name = scanner.nextLine();
        System.out.println("Введите Фамилию");
        String surname = scanner.nextLine();
        System.out.println("Введите Должность");
        String appointment = scanner.nextLine();
        System.out.println("Введите Зарплату");
        int salary = scanner.nextInt();
        Employee e = new Employee(name, surname, appointment, salary);
        list.add(e);
    }


    public void conclusionEmployee() {
        if (list.isEmpty()) {
            System.out.println("Данных о сотрудниках нет.");
        }
        for (Employee emp : list) {
            System.out.println(emp.getName() + " " + emp.getSurname() + " " + emp.getAppointment() + " " + emp.getSalary());
        }
    }

    public  void searchAppointment(EmployeeList list) {
        System.out.println("Введите должность: ");
        Scanner appointments = new Scanner(System.in);
        String app = appointments.nextLine();
        list.findByAppointment(app);
    }

    private void findByAppointment(String appointment) {
        for (Employee e : list) {
            if (e.getAppointment().equalsIgnoreCase(appointment)) {
                System.out.println(e.getName() + " " + e.getSurname());
            }
        }
    }

    public void sort(Comparator<Employee> comp) {
        list.sort(comp);
        for (Employee emp : list) {
            System.out.println(emp.getName() + " " + emp.getSurname() + " " + emp.getAppointment() + " " + emp.getSalary());
        }
    }
}


